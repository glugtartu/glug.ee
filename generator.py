import os
import shutil
import pathlib

TEMPLATE_FILE = "template.html"
CONTENT_DIR = "content/"
STATIC_DIR = "static/"
DIST_DIR = "dist/"
DEFAULT_NAV_FILE = "nav.html"
LANG_NAVS = {
    "en": "nav_en.html"
}

# Move static files to dist directory
for static_file in os.listdir(STATIC_DIR):
    shutil.copy(STATIC_DIR + static_file, DIST_DIR)

# Generate HTML files using template and content
with open(TEMPLATE_FILE) as file:
    template = file.read()

# Recursively find HTML files in content directory
for file in pathlib.Path(CONTENT_DIR).rglob("*.html"):
    content_file_path = str(file)
    dist_file_path = content_file_path.replace(CONTENT_DIR, DIST_DIR)
    dist_file_dir = os.path.dirname(dist_file_path)
    parent_dir = dist_file_dir[len(DIST_DIR.rstrip("/")):] or "/"

    lang = parent_dir.split("/")[1]
    nav_file_path = LANG_NAVS[lang] if lang in LANG_NAVS else DEFAULT_NAV_FILE

    with open(content_file_path) as content_file:
        content = content_file.read()

    with open(nav_file_path) as nav_file:
        nav = nav_file.read()

    # Make directories in the dist directory in case they don't exist
    pathlib.Path(dist_file_dir).mkdir(parents=True, exist_ok=True)

    # Replace content with HTML from content file
    dist_file_content = template.replace("{{ content }}", content)

    # Replace nav with HTML from nav file
    dist_file_content = dist_file_content.replace("{{ nav }}", nav)

    # Replace path in title with parent directory
    dist_file_content = dist_file_content.replace("{{ path }}", parent_dir)

    with open(dist_file_path, "w") as dist_file:
        dist_file.write(dist_file_content)

