import markdown
import os
import shutil
import re

www_dir = "/var/www/glug.ee/html/"
static_dir = "static/"
pages_dir = "pages/"
template_file = "template.html"

copy_files = os.listdir(static_dir)
for copy_file in copy_files:
    shutil.copy(static_dir + copy_file, www_dir)

with open("template.html") as f:
    template = f.read()


for page_file in os.listdir(pages_dir):
    page_name = re.sub("\.md$", "", page_file) + ".html"

    with open(pages_dir + page_file) as f:
        page_md = f.read()

    page_title = page_md.splitlines()[0].lstrip(" #")

    body_html = markdown.markdown(page_md, output_format="html5")

    body_line = [line for line in template.splitlines() if "{{ body }}" in line][0]
    indent = len(body_line) - len(body_line.lstrip(" "))

    body_html = "\n".join(" " * indent + line for line in body_html.splitlines())

    page_html = template.replace(body_line, body_html)
    page_html = page_html.replace("{{ title }}", page_title)

    with open(www_dir + page_name, "w") as f:
        f.write(page_html)
